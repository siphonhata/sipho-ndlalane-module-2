// Defining class
class App {
  String? appName;
  int? yearWon;
  String? category;
  String? devloper;

  // defining class function transform
  String? transformAppName(String? name) {
    name = (appName.toString()).toUpperCase();

    return name;
  }
}

void main() {
  var std = new App(); //Object
  std.appName = "Takealot app";
  std.category = "Consumer Solution";
  std.devloper = "Kim Reid";
  std.yearWon = 2021;

  String? name = std.transformAppName(std.appName);

  print("App Name\tSector/Category\t\tDeveloper\tYear Won");
  print("----------------------------------------------------------------");
  print("${name}\t${std.category}\t${std.devloper}\t  ${std.yearWon}");
}
