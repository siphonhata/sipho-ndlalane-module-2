import 'dart:io';

void main() {
  List<String> winApp = [
    "FNB Banking App",
    "Bookly",
    "Supersport",
    "DSTV Now",
    "Ikhokha",
    "Shyft",
    "Besmarta",
    "Hydra",
    "Easy Equities",
    "Takealot"
  ];

  List<int> year = [2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021];

  winApp.sort();
  print("\nSorted list of App\n");
  for (int i = 0; i < winApp.length; i++) {
    print(winApp[i]);
  }

  //print winin app in 2017 n 2018
  int index1 = 0, index2 = 0;
  for (int i = 0; i < year.length; i++) {
    if (year[i] == 2017) {
      index1 = i;
      break;
    }
  }
  for (int i = 0; i < year.length; i++) {
    if (year[i] == 2018) {
      index2 = i;
      break;
    }
  }
  print("\nThe winning App for 2017 is: ${winApp[index1]}");
  print("The winning App for 2018 is: ${winApp[index2]}");

  print("\nThe total number of apps is: ${winApp.length}");
}
